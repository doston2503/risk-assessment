import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Home from "./pages/home/Home";
import {ToastContainer} from "react-toastify";
import Complaints from "./layouts/complaints/Complaints";
import CreditRisk from "./layouts/creditRisk/CreditRisk";
import MarketRisk from "./layouts/marketRisk/MarketRisk";
import ExternalApi from "./layouts/externalApi/ExternalApi";
import Blank from "./layouts/blanks/Blank";
import Collection from "./layouts/collection/Collection";
import Compliance from "./layouts/compliance/Compliance";
import ProblemAssets from "./layouts/problemAssets/ProblemAssets";
import Login from "./pages/login/Login";
import Registration from "./pages/registration/Registration";
import 'react-toastify/dist/ReactToastify.css';
import ProtectedPath from "./components/ProtectedPath";
import Application from "./layouts/application/Application";
import Verification from "./pages/verification/Verification";
function App(props) {
    return (
        <div>
            <BrowserRouter>
                <Switch>
                    <Route exact path="/login" component={Login}/>
                    <Route exact path="/register" component={Registration}/>
                    <Route exact path="/verification" component={Verification}/>
                    <ProtectedPath>
                        <Route exact path="/" component={Home}/>
                        <Route path="/creditRisk" component={CreditRisk}/>
                        <Route path="/marketRisk" component={MarketRisk}/>
                        <Route path="/externalApi" component={ExternalApi}/>
                        <Route path="/blanks" component={Blank}/>
                        <Route path="/collection" component={Collection}/>
                        <Route path="/complaints" component={Complaints}/>
                        <Route path="/compliance" component={Compliance}/>
                        <Route path="/problemAssets" component={ProblemAssets}/>
                        <Route path="/application" component={Application}/>
                    </ProtectedPath>
                    {/*   <Route exact path="/"><Redirect to="/main" /></Route>
                <Route path="/main" component={MainLayout}/>
                <Route component={NotFound}/>*/}
                </Switch>
            </BrowserRouter>
            <ToastContainer/>
        </div>
    );
}

export default App;