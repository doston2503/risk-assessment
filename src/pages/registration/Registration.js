import React from 'react';
import {Link} from "react-router-dom";
import axios from "axios";
import {PATH_NAME} from "../../utils/utils";
import {toast} from "react-toastify";

function Registration(props) {

    function registration(e) {
        e.preventDefault();
        let username = e.target.username.value;
        let password1 = e.target.password1.value;
        let password2 = e.target.password2.value;
        let phone = e.target.phone.value;

        try {

            axios.post(`${PATH_NAME}api/v1/users/registration/`, {
                "username": username,
                "password1": password1,
                "password2": password2,
                "phone": phone,
            }).then((response) => {
                 props.history.push('/login');
            })


        } catch (e) {
            toast.error("Xato ma'lumot kiritilgan")
        }
    }

    return (
        <div className="register-page">
            <div className="box">
                <b>Risk Assessment System</b>
                <p className='opacity-75'>Зарегистрироваться в системе</p>
                <div className="card">
                    <form onSubmit={registration}>
                        <label htmlFor="phone">Мобильный телефон*</label>
                        <input
                            className="form-control mt-2"
                            name={'phone'}
                            id={'phone'}
                            type="number"/>
                        <small className="mb-3 opacity-75 d-block ">
                            Без символа "+", например: 998XXAAABBCC
                        </small>


                        <label htmlFor="userName">Username*</label>
                        <input
                            className="form-control mt-2"
                            name={'username'}
                            id={'userName'}
                            type="text"/>
                        <small className="mb-3 opacity-75 d-block ">
                            Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.
                        </small>


                        <label htmlFor="password">Password*</label>
                        <input
                            className="form-control mt-2"
                            name={'password1'}
                            id={'password'}
                            type="password"/>
                        <ul>
                            <li>Your password must contain at least 8 characters.</li>
                            <li>Your password can’t be a commonly used password.</li>
                            <li>Your password can’t be entirely numeric.</li>
                        </ul>

                        <label htmlFor="re-password">Password confirmation*</label>
                        <input
                            className="form-control mt-2"
                            name={'password2'}
                            id={'re-password'}
                            type="password"/>
                        <small className='opacity-75'>Enter the same password as before, for verification.</small>

                        <div className="d-flex mt-4 footer-card">
                            <button className="btn btn-light">
                                <Link to={'/login'}>Назад к входу</Link>
                            </button>
                            <button
                                type={'submit'}
                                className="btn btn-primary ms-3">Регистрация
                            </button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    );
}
export default Registration;