import React from 'react';
import {Link} from "react-router-dom";

function ComplaintsListTable(props) {
    return (
        <div>
            <div className="d-flex justify-content-between mb-4">
                <div className="search-filter">
                    <div className="search-input">
                        <input
                            placeholder={' Поиск ...'}
                            type="search"/>
                        <img src="/assets/table/search.png" alt=""/>
                    </div>
                    <div className="filter-data">
                        <img src="/assets/table/filter.png" alt=""/>
                        Фильтр
                    </div>
                </div>
                <button className="create-btn">
                    <div className="circle">
                        <span className="icon icon-plus"/>
                    </div>
                    Создать новое обращение
                </button>
            </div>
            <div className="overflow-auto">
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <th>LINK</th>
                        <th>СТАТУС</th>
                        <th>ТИП ОБРАЩЕНИЯ</th>
                        <th>ТЕМА</th>
                        <th>ТЕКСТ ОБРАЩЕНИЯ</th>
                        <th>ДАТА ОТПРАВЛЕНИЯ</th>
                        <th>ФИЛИАЛ ОТПРАВИТЕЛЯ</th>
                        <th>ДЕПАРТАМЕНТ ОТПРАВИТЕЛЯ</th>
                        <th>КОМУ</th>
                        <th>ЭТАП</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><Link to='/complaints/list/data/1'>CASE-1779</Link></td>
                        <td>
                            <div className="status status-income">
                                Отправлено
                            </div>
                        </td>
                        <td> Запрос для доступа в ИАБС</td>
                        <td>Лимитга запрос жунатиб булмаяпти</td>
                        <td>Лимитга хат жунатишда хатолик юз бермокд ...</td>
                        <td>Sept. 6, 2022, 5:23 p.m</td>
                        <td>Каршинский</td>
                        <td>Кичик бизнесга хизмат кўрсатиш департаменти</td>
                        <td>Департамент Рисков</td>
                        <td>НИКИ</td>
                    </tr>
                    <tr>
                        <td><Link to='/complaints/list/data/2'>CASE-1779</Link></td>
                        <td>
                            <div className="status status-confirmation">
                                Отправлено на согласовании
                            </div>
                        </td>
                        <td> Запрос для доступа в ИАБС</td>
                        <td>Лимитга запрос жунатиб булмаяпти</td>
                        <td>Лимитга хат жунатишда хатолик юз бермокд ...</td>
                        <td>Sept. 6, 2022, 5:23 p.m</td>
                        <td>Каршинский</td>
                        <td>Кичик бизнесга хизмат кўрсатиш департаменти</td>
                        <td>Департамент Рисков</td>
                        <td>НИКИ</td>
                    </tr>
                    <tr>
                        <td><Link to='/complaints/list/data/1'>CASE-1779</Link></td>
                        <td>
                            <div className="status status-decided">
                                Решено
                            </div>
                        </td>
                        <td> Запрос для доступа в ИАБС</td>
                        <td>Лимитга запрос жунатиб булмаяпти</td>
                        <td>Лимитга хат жунатишда хатолик юз бермокд ...</td>
                        <td>Sept. 6, 2022, 5:23 p.m</td>
                        <td>Каршинский</td>
                        <td>Кичик бизнесга хизмат кўрсатиш департаменти</td>
                        <td>Департамент Рисков</td>
                        <td>НИКИ</td>
                    </tr>
                    <tr>
                        <td><Link to='/complaints/list/data/2'>CASE-1779</Link></td>
                        <td>
                            <div className="status status-rejected">
                                Отклонено
                            </div>
                        </td>
                        <td> Запрос для доступа в ИАБС</td>
                        <td>Лимитга запрос жунатиб булмаяпти</td>
                        <td>Лимитга хат жунатишда хатолик юз бермокд ...</td>
                        <td>Sept. 6, 2022, 5:23 p.m</td>
                        <td>Каршинский</td>
                        <td>Кичик бизнесга хизмат кўрсатиш департаменти</td>
                        <td>Департамент Рисков</td>
                        <td>НИКИ</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default ComplaintsListTable;