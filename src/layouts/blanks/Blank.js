import React from 'react';

function Blank(props) {
    return (
        <div className='mainLayout'>
            <div className="mainLayoutLeft"></div>
            <div className="mainLayoutRight"></div>
        </div>
    );
}

export default Blank;