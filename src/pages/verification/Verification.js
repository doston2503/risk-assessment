import React from 'react';
import {Link} from "react-router-dom";

function Verification(props) {
    return (
        <div className="login-page">
            <div className="box">
                <b>Risk Assessment System</b>
                <div className="card">
                    <form>
                        <label htmlFor="password">Verification*</label>
                        <input
                            required={true}
                            className="form-control mt-2 mb-3"
                            name={'verification'}
                            id={'verification'}
                            type="text"/>
                        <button type={'submit'} className="d-block w-100 mt-4 btn btn-primary">
                            Верификация
                        </button>
                    </form>
                </div>
            </div>

        </div>
    );
}

export default Verification;