import React, {useEffect, useRef, useState} from 'react';
import {Link} from "react-router-dom";
import axios from "axios";
import {PATH_NAME} from "../../../utils/utils";
import {toast} from "react-toastify";

function Dashboard(props) {
    const [allData, setAllData] = useState([]);
    const [typeApplication, setTypeApplication] = useState('co_credit');
    const [id, setId] = useState('');
    const [inn, setInn] = useState('');
    const [dataId, setDataId] = useState([]);
    const [dataINN, setDataINN] = useState([]);
    const [loading, setLoading] = useState(false);
    const idRef = useRef(null);
    const innRef = useRef(null);
    let token = localStorage.getItem('token');

    function handleSearch() {

        if (typeApplication === 'co_credit') {
            let id = idRef.current.value;
            setLoading(true);
            axios.get(`${PATH_NAME}api/v1/pledges/get_claim?claim_id=${id}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
                .then((res) => {
                    setDataId(res.data);
                    setId(idRef.current.value);

                    setLoading(false);
                }).catch((e) => {
                setLoading(false);
                toast.error(e.response?.data?.detail)
            })
        } else {
            let inn = innRef.current.value;
            setLoading(true);
            axios.get(`${PATH_NAME}api/v1/pledges/get_client?inn=${inn}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
                .then((res) => {
                    setInn(innRef.current.value);
                    setDataINN(res.data);
                    setLoading(false);
                }).catch((e) => {
                setLoading(false);
                toast.error(e.response?.data?.detail)
            })
        }
    }

    useEffect(() => {
        axios.get(`${PATH_NAME}api/v1/pledges/applications/list`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then((res) => {
                setAllData(res.data);
            })
    }, []);


    function addApplication() {
        axios.post(`${PATH_NAME}api/v1/pledges/applications/create`,
            typeApplication === "co_credit" ?
                {
                    operation_type: typeApplication,
                    claim_id: parseInt(id),
                } :
                {
                    operation_type: typeApplication,
                    client_inn: parseInt(inn)
                }
            , {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then((res) => {
            setId('');
            setInn('');
            setDataINN([]);
            setDataId([]);
            idRef.current.value='';
            innRef.current.value='';
            setTypeApplication('co_credit');

            axios.get(`${PATH_NAME}api/v1/pledges/applications/list`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
                .then((res2) => {
                    console.log(res2.data)
                    setAllData(res2.data);
                })

            // setAllData(res.data)
        })
    }

    return (
        <div className='application-dashboard-page'>
            <h5>ОБРАЩЕНИЯ</h5>
            <div className="container-fluid">
                <div className="row my-3">
                    <div className="col-xl-12">
                        <div className="card border-0">
                            <div className="card-header p-3 bg-transparent border-bottom-0">
                                <button
                                    className="create-application-btn"
                                    data-bs-toggle="offcanvas" data-bs-target="#demo">
                                    <span className="icon icon-plus"/>
                                    Поиск обращение
                                </button>
                            </div>
                            <div className="card-body">
                                <table className="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Последнее изменение</th>
                                        <th>Тип операции</th>
                                        <th>Клиент</th>
                                        <th>Сумма</th>
                                        <th>Действие</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    {allData?.map((item, index) => (
                                        <tr key={index}>
                                            <td>{item.id}</td>

                                            <td> {item.last_update.substring(0, item.last_update.length - 3)}</td>
                                            <td>
                                                {
                                                    item.operation_type === 'co_credit' ? 'Кредит' :
                                                        item.operation_type === 'co_letter_of_credit' ? 'Аккредитив' :
                                                            item.operation_type === 'co_warranty' ? 'Банковская гарантия' :
                                                                item.operation_type === 'release' ? 'Замена залога / высвобождение' :
                                                                    'Принятие на баланс банка'
                                                }
                                            </td>
                                            <td>
                                                <Link className="text-decoration-none" to={'#'}>
                                                    {item.client.name}
                                                </Link>
                                            </td>
                                            <td>{item.sum ? item.sum : 0} USD</td>
                                            <td>
                                                <Link to={`/application/${item.id}`} className="btn btn-sm  btn-link">
                                                    Просмотреть
                                                </Link>
                                            </td>
                                        </tr>
                                    ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="offcanvas offcanvas-end" id="demo">
                <div className="offcanvas-header">
                    <h5 className="offcanvas-title">
                        Запрос на обращение
                    </h5>
                    <button type="button" className="btn-close text-reset btn-close-white" data-bs-dismiss="offcanvas"/>
                </div>
                <div className="offcanvas-body">
                    <label htmlFor="operation_type">Тип обращения</label>
                    <select
                        onChange={(e) => setTypeApplication(e.target.value)}
                        className="form-select" name="operation_type" id="operation_type">

                        <option value="co_credit">Кредит</option>

                        <option value="co_letter_of_credit">Аккредитив</option>

                        <option value="co_warranty">Банковская гарантия</option>

                        <option value="release">Замена залога / высвобождение</option>

                        <option value="balance_acceptance">Принятие на баланс банка</option>

                    </select>

                    <div className="d-flex mt-3">
                        {typeApplication === 'co_credit' ?
                            <input
                                ref={idRef}
                                placeholder={'Введите claim_id'}
                                name={'claim_id'}
                                className="form-control"
                                type="search"/>
                            :
                            <input
                                ref={innRef}
                                placeholder={'Введите инн'}
                                name={'claim_id'}
                                className="form-control"
                                type="search"/>
                        }
                        <button
                            onClick={handleSearch}
                            className="btn btn-primary">Поиск
                        </button>
                    </div>

                    {loading ? <span className="spinner-border"/>
                        :
                        <>
                            {(typeApplication === 'co_credit' && id !== '') ?

                                <>
                                    <table className="table mt-5">
                                        <tbody>
                                        <tr>
                                            <td className="fw-bold">Client name</td>
                                            <td>{dataId?.claim?.client_name}</td>
                                        </tr>
                                        <tr>
                                            <td className="fw-bold">Loan amount</td>
                                            <td>{dataId?.claim?.summ_claim}</td>
                                        </tr>
                                        <tr>
                                            <td className="fw-bold">Loan type</td>
                                            <td>{dataId?.claim?.loan_type}</td>
                                        </tr>
                                        <tr>
                                            <td className="fw-bold">Client INN</td>
                                            <td>{dataId?.claim?.inn}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <button data-bs-toggle="offcanvas" data-bs-target="#demo" onClick={addApplication}
                                            className="btn btn-success w-100 mt-5">
                                        Отправить
                                    </button>
                                </>

                                : (typeApplication !== '' && inn !== '') ?
                                    <>
                                        <table className="table mt-5">
                                            <tbody>
                                            <tr>
                                                <td className="fw-bold">Client name</td>
                                                <td>{dataINN?.name}</td>
                                            </tr>
                                            <tr>
                                                <td className="fw-bold">Client phone</td>
                                                <td>{dataINN?.phone}</td>
                                            </tr>
                                            <tr>
                                                <td className="fw-bold">INN</td>
                                                <td>{dataINN?.inn}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <button data-bs-toggle="offcanvas" data-bs-target="#demo"
                                                onClick={addApplication} className="btn btn-success w-100 mt-5">
                                            Отправить
                                        </button>
                                    </>
                                    : ''
                            }

                        </>

                    }

                </div>
            </div>

        </div>
    );
}

export default Dashboard;