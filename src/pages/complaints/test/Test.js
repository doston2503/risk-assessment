import React, {useState, useEffect} from 'react';
import Images from "./Images";


function Test(props) {
    const [image, setImage] = useState([]);
    const [loading, setLoading] = useState(false);
    const [search, setSearch] = useState('');

    useEffect(() => {
        setLoading(true);
        fetch('https://jsonplaceholder.typicode.com/photos')
            .then(response => response.json()
                .then((data) => {
                    setImage(data);
                    setLoading(false)
                }));

    }, []);

    return (
        <div className="container-fluid">

            <div className="row ">
                <div className="col-xl-4">
                    <input
                        onChange={(e) => setSearch(e.target.value)}
                        className="form-control"
                        name="title"
                        placeholder="enter title"
                        type="search"/>
                </div>
                <div className="col-xl-12">
                    <table className="table table-bordered">
                        <thead>
                        <tr>
                            <th>TR</th>
                            <th>Title</th>
                            <th>Img</th>
                        </tr>
                        </thead>
                        {loading ? <span className='spinner-border'/> :
                            <Images search={search} items={image}/>
                        }

                    </table>
                </div>
            </div>
        </div>
    );
}

export default Test;