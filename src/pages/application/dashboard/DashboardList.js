import React from 'react';
import {Link} from "react-router-dom";

function DashboardList(props) {
    return (
        <div className="application-dashboard-list-page">
            <div className="container-fluid">
                <div className="row list-header">
                    <div className="col-xl-12">
                        <h5>Детали обращения</h5>
                    </div>
                    <div className="col-xl-12">
                        <div className="card">
                            <div className="card-body d-flex justify-content-between">
                                <div className="client">
                                    <div className="title">
                                        Заемщик / клиент
                                    </div>
                                    <div className="client-name">
                                        <Link to={'#'} className="text-decoration-none">
                                            Пак Павел Александрович
                                        </Link>
                                    </div>

                                </div>
                                <div className="date">
                                    <div className="title">
                                        Дата и время
                                    </div>
                                    <b>
                                        Sept. 16, 2022, 10 a.m.
                                    </b>
                                </div>
                                <div className="author">
                                    <div className="title">
                                        Автор обращения
                                    </div>
                                    <b>
                                        admin
                                    </b>
                                </div>
                                <div className="status">
                                    <div className="title">
                                        Статус
                                    </div>
                                    <button className="btn btn-primary disabled btn-sm">
                                        Черновик
                                    </button>
                                </div>
                                <button className="px-3">
                                    <span className="icon icon-date"/>
                                    История
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row mt-4 list-body">
                    <div className="col-xl-12">
                        <h5>Состав обращения</h5>
                    </div>
                    <div className="col-xl-12">
                        <div className="card border-0">
                            <div className="card-body">
                                <div className="row mb-3">
                                    <div className="col-xl-6">
                                        <label htmlFor="operation_type">Тип обращения</label>
                                        <select className="form-select" name="operation_type" id="operation_type"
                                                required={true}>

                                            <option value="co_credit">Credit</option>

                                            <option value="co_letter_of_credit">Letter of credit</option>

                                            <option value="co_warranty">Bank warranty</option>

                                            <option value="release">Replacement of collateral / release</option>

                                            <option value="balance_acceptance">Acceptance to the bank's balance
                                            </option>

                                        </select>
                                    </div>
                                    <div className="col-xl-6">
                                        <label>Валюта</label>
                                        <select className="form-select" name="credit_currency" required={true}>

                                            <option value="sum" >СУМ</option>

                                            <option value="usd">USD</option>

                                            <option value="euro">EURO</option>

                                        </select>
                                    </div>
                                </div>
                                <div className="row mb-3">
                                    <div className="col-xl-12">
                                        <label htmlFor="credit_sum">credit operation purpose</label>
                                        <input className="form-control" type="text" placeholder="Напр.: покупка авто"
                                               name="operation_purpose" id="operation_purpose"
                                               required={true}/>
                                    </div>
                                </div>
                                <div className="row mb-3">
                                    <div className="col-lg-6">
                                        <div className="mb-3">
                                            <label htmlFor="credit_sum">Сумма</label>
                                            <input className="form-control" type="number" placeholder="Введите сумму"
                                                   id="credit_sum" name="credit_sum" required={true}/>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="mb-3">
                                            <label htmlFor="credit_term">Срок в месяцах</label>
                                            <input className="form-control" type="number" placeholder="Введите срок"
                                                   id="credit_term" name="credit_term" required={true}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mb-3">
                                    <div className="col-lg-6">
                                        <div className="mb-3">
                                            <label htmlFor="review_authority">Review authority</label>

                                            <select className="form-select" name="review_authority"
                                                    id="review_authority" required="">

                                                <option value="branch_authority">Branch rights</option>

                                                <option value="head_office">Head office</option>

                                            </select>

                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="mb-3">
                                            <label htmlFor="initiating_department">Initiating department</label>

                                            <select className="form-select" name="initiating_department"
                                                    id="initiating_department" required="">

                                                <option value="small_business" >Small Business Service
                                                    Department
                                                </option>

                                                <option value="corporate_service">Corporate Services Department</option>

                                                <option value="credit_monitoring">Credit Monitoring Department</option>

                                                <option value="distressed_assets">Department for Dealing with Distressed
                                                    Assets
                                                </option>

                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default DashboardList;