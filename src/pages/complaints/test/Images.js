import React, {useState, useEffect} from 'react';
import ReactPaginate from "react-paginate";
function Images(props) {
    const {items} = props;
    const itemsPerPage = 10;

    const [currentItems, setCurrentItems] = useState([]);
    const [pageCount, setPageCount] = useState(0);
    const [itemOffset, setItemOffset] = useState(0);

    useEffect(() => {
        const endOffset = itemOffset + itemsPerPage;
        setCurrentItems(items.slice(itemOffset, endOffset));
        setPageCount(Math.ceil(items.length / itemsPerPage));
    }, [itemOffset, itemsPerPage,items]);

    const handlePageClick = (event) => {
        const newOffset = (event.selected * itemsPerPage) % items.length;
        setItemOffset(newOffset);
    };


    return (
        <>
            <tbody>
            {currentItems.map((val, index) => (
                <tr key={index}>
                    <td>{val.id}</td>
                    <td>{val.title}</td>
                    <td>
                        <img style={{width: '20px'}} src={val.url} alt=""/>
                    </td>
                </tr>
            ))}
            </tbody>
            <ReactPaginate
                breakLabel="..."
                nextLabel="next >"
                onPageChange={handlePageClick}
                pageRangeDisplayed={5}
                pageCount={pageCount}
                previousLabel="< previous"
                renderOnZeroPageCount={null}
                containerClassName={'pagination'}
                previousLinkClassName={'page-num'}
                nextLinkClassName={'page-num'}
                activeLinkClassName={'link-active'}
            />
        </>
    );
}

export default Images;