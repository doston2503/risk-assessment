import React from 'react';
import ComplaintsListTable from "../../../table/ComplaintsListTable";

function List(props) {
    return (
        <div className="container-fluid complaints-list">
            <div className="row">
                <div className="col-xl-12">
                    <h5>Все обращения</h5>
                </div>
                <div className="col-xl-12">
                    <div className="card border-0">
                        <div className="card-body">
                            <ComplaintsListTable/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default List;