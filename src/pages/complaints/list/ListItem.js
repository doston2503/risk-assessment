import React from 'react';

function ListItem(props) {
    return (
        <div className="complaints-list-item-page">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xl-12">
                        <h5>Карточка обращения №1,812</h5>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-xl-8">
                        <ul className="nav nav-tabs">
                            <li className="nav-item">
                                <a className="nav-link active" data-bs-toggle="tab" href="#menu1">Информация</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-bs-toggle="tab" href="#menu2">Все документы</a>
                            </li>
                        </ul>

                        <div className="tab-content">
                            <div className="tab-pane active" id="menu1">
                                <div className="card border-0">
                                    <div className="card-body">
                                        <h5 className="mb-3">Общая информация</h5>
                                        <div className="row">
                                            <div className="col-xl-6">
                                                <h6>Филиал отправителя</h6>
                                                <p className="text-info">Наманганский</p>
                                                <h6>Департамент отправителя</h6>
                                                <p className="text-info">Кредит мониторинги департаменти</p>
                                                <h6>Пользователь</h6>
                                                <p className="text-info">Rajabov Doston</p>
                                            </div>
                                            <div className="col-xl-6">
                                                <h6>Кому</h6>
                                                <p className="text-info">Департамент розничного бизнеса</p>
                                                <h6>Тип обращения</h6>
                                                <p className="text-info">Реквизиты-обеспечение</p>
                                                <h6>Приоритет</h6>
                                                <p className="text-info">Высокий</p>
                                            </div>
                                        </div>
                                        <hr/>
                                        <h5 className="mb-3">Детали обращения</h5>
                                        <div className="row">
                                            <div className="col-xl-12">
                                                <h6>Тема</h6>
                                                <p className="text-info">Надо изменить Имущественная права на Транспортные средства</p>
                                                <h6>Текст обращения</h6>
                                                <p className="text-info">Гаров ўзгартириш</p>
                                                <h6>Вложения</h6>
                                                <a href="#" className="text-info">
                                                    Рискка илова.xlsx
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="menu2">
                                <div className="card border-0">
                                    <div className="card-body">
                                        <table className="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>ТИП</th>
                                                <th>НАИМЕНОВАНИЕ</th>
                                                <th>РАЗМЕР</th>
                                                <th>ДЕПАРТАМЕНТ</th>
                                                <th>СОЗДАЛ</th>
                                                <th>ДАТА</th>
                                                <th>ОПИСАНИЕ</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="col-xl-4">
                        <div className="card border-0 process-card">
                            <div className="card-body">
                                <h5>Процесс обращения</h5>
                                <div className="message">
                                    <span className="icon icon-message"/>
                                    <span>
                                         Обращение отправлено в Департамент розничного бизнеса
                                    </span>
                                </div>
                                <table className="table table-bordered">
                                    <tr>
                                        <th>Дата отправления</th>
                                        <td>Sept. 7, 2022, 11:25 a.m.</td>
                                    </tr>
                                    <tr>
                                        <th>Дата изменения</th>
                                        <td>Sept. 7, 2022, 11:25 a.m</td>
                                    </tr>
                                    <tr>
                                        <th>Дата закрытия</th>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <th>Статус</th>
                                        <td>Отправлено</td>
                                    </tr>
                                </table>
                                <div className="history-attempt">
                                    <b>История действии</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ListItem;