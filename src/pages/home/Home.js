import React from 'react';
import {Link} from "react-router-dom";
import axios from "axios";
import {PATH_NAME} from "../../utils/utils";
import {toast} from "react-toastify";

function Home(props) {
    function logOut() {
        let token = localStorage.getItem('token');

        axios.post(`${PATH_NAME}api/v1/auth/logout`, null, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then((res) => {
                console.log(res);
                toast.success('ok');
                localStorage.removeItem('token');
                props.history.push('/login')
            })
    }

    return (
        <div className="home-page">
            <div className="home-page-header">
                <div className="container">
                    <div className="d-flex justify-content-between">
                        <Link className="navbar-brand font-weight-bold" to="/">
                            <span className="icon icon-pulse"/>
                            Risk Assessment System
                        </Link>
                        <div className="btns-group">
                            <button onClick={logOut}>
                                Logout
                            </button>
                            <button>
                                <span className="icon icon-notification"/>
                            </button>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-xl-8 mx-auto">
                            <h1>Risk Assessment System</h1>
                            <p>
                                Risk Assessment System is developed to help financial
                                institutions optimize risk infrastructures with
                                regulatory calculations, enhanced with real-time
                                analytics and comprehensive reporting.
                            </p>
                        </div>
                    </div>
                </div>

            </div>
            <div className="home-page-body">

                <div className="container">
                    <div className="d-flex justify-content-between">
                        <Link to="/creditRisk" className="btn btn-lg btn-pill btn-link text-dark">Кредитные риски</Link>
                        <Link to="/marketRisk" className="btn btn-lg btn-pill btn-link text-dark">Риски
                            ликвидности</Link>


                        <Link to="/externalApi" className="btn btn-lg btn-pill btn-link text-dark">API сервисы</Link>
                        <Link to="/blanks" className="btn btn-lg btn-pill btn-link text-dark">Заявления - анкета</Link>


                        <Link to="/collection" className="btn btn-lg btn-pill btn-link text-dark">Collection</Link>


                        <Link to="/complaints" className="btn btn-lg btn-pill btn-link text-dark">Service Desk</Link>


                        <Link to="/compliance" className="btn btn-lg btn-pill btn-link text-dark">Compliance</Link>


                        <Link to="/problemAssets" className="btn btn-lg btn-pill btn-link text-dark">Проблемные
                            активы</Link>

                        <Link to="/application" className="btn btn-lg btn-pill btn-link text-dark">Обращения
                        </Link>
                    </div>

                </div>
            </div>


        </div>
    );
}

export default Home;