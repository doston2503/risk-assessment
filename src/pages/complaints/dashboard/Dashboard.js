import React, {useEffect, useState} from 'react';
import {PATH_NAME} from "../../../utils/utils";
import axios from "axios";

function Dashboard(props) {
    const [data, setData] = useState([]);


    useEffect(() => {
        let token = localStorage.getItem('token');

        axios.get(`${PATH_NAME}v1/api/apps/complaints/`, {
            headers: {
                Authorization:`Bearer ${token}`
            }
        })
            .then((res) => {
                setData(res.data);
            })

    }, []);

    return (
        <div className="complaints-dashboard-page">
            <div className="container-fluid">
                <div className="row all-statistics">
                    <div className="col-xl-12">
                        <div className="d-flex flex-wrap">
                            <div className="card">
                                <div className="count">{data.count_all}</div>
                                <div className="text">Все обращения</div>
                            </div>

                            <div className="card">
                                <div className="count">{data.count_reviews}</div>
                                <div className="text">Рассмотрение</div>
                            </div>
                            <div className="card">
                                <div className="count">{data.count_decides}</div>
                                <div className="text">Решено</div>
                            </div>
                            <div className="card">
                                <div className="count">{data.count_rejects}</div>
                                <div className="text">Отклонено</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row my-4">
                    <div className="col-xl-12">
                        <ul className="nav nav-tabs">
                            <li className="nav-item">
                                <a className="nav-link active" data-bs-toggle="tab" href="#menu1">
                                    В разбивке по типам обращения
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-bs-toggle="tab" href="#menu2">
                                    В разбивке по филиалам
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-bs-toggle="tab" href="#menu3">
                                    В разбивке по департаментам
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-bs-toggle="tab" href="#menu4">
                                    В разбивке по типам отказа
                                </a>
                            </li>
                        </ul>

                        {/*<div className="tab-content mt-3">
                            <div className="tab-pane active" id="menu1">
                                <div className="card border-0">
                                    <div className="card-body">
                                        <div className="overflow-auto">
                                            <table className="table table-hover table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>ТИП ОБРАЩЕНИЯ</th>
                                                    <th>КОЛ-ВО ОБРАЩЕНИЙ</th>
                                                    <th>РЕШЕННЫЕ</th>
                                                    <th>ОТКЛОНЕННЫЕ</th>
                                                    <th>РАССМОТРЕНИЕ</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {data?.by_types_table?.map((item, index) => (
                                                    <tr key={index}>
                                                        <td>{item.case_type}</td>
                                                        <td>{item.total}</td>
                                                        <td>{item.solved}</td>
                                                        <td>{item.rejected}</td>
                                                        <td>{item.process}</td>
                                                    </tr>
                                                ))}


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="menu2">
                                <div className="card border-0">
                                    <div className="card-body">
                                        <div className="overflow-auto">
                                            <table className="table table-hover table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>ФИЛИАЛ</th>
                                                    <th>КОЛ-ВО ОБРАЩЕНИЙ</th>
                                                    <th>РЕШЕННЫЕ</th>
                                                    <th>ОТКЛОНЕННЫЕ</th>
                                                    <th>РАССМОТРЕНИЕ</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {data?.by_branches_table?.map((item, index) => (
                                                    <tr key={index}>
                                                        <td>{item.branch}</td>
                                                        <td>{item.total}</td>
                                                        <td>{item.solved}</td>
                                                        <td>{item.rejected}</td>
                                                        <td>{item.process}</td>
                                                    </tr>
                                                ))}

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="menu3">
                                <div className="card border-0">
                                    <div className="card-body">
                                        <div className="overflow-auto">
                                            <table className="table table-hover table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>ДЕПАРТАМЕНТ</th>
                                                    <th>КОЛ-ВО ОБРАЩЕНИЙ</th>
                                                    <th>РЕШЕННЫЕ</th>
                                                    <th>ОТКЛОНЕННЫЕ</th>
                                                    <th>РАССМОТРЕНИЕ</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {data?.by_departments_table?.map((item, index) => (
                                                    <tr key={index}>
                                                        <td>{item.department}</td>
                                                        <td>{item.total}</td>
                                                        <td>{item.solved}</td>
                                                        <td>{item.rejected}</td>
                                                        <td>{item.process}</td>
                                                    </tr>
                                                ))}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="menu4">
                                <div className="card border-0">
                                    <div className="card-body">
                                        <div className="overflow-auto">
                                            <table className="table table-hover table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>ТИП ОТКАЗА</th>
                                                    <th>КОЛ-ВО ОБРАЩЕНИЙ</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {data?.by_reject_type_table?.map((item, index) => (
                                                    <tr key={index}>
                                                        <td>{item.name}</td>
                                                        <td>{item.total}</td>

                                                    </tr>
                                                ))}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>*/}
                    </div>
                </div>

            </div>
        </div>
    );
}

export default Dashboard;