import React from 'react';
import ComplaintsListTable from "../../../table/ComplaintsListTable";

function ListRejected(props) {
    return (
        <div>
            <div className="container-fluid complaints-list">
                <div className="row">
                    <div className="col-xl-12">
                        <h5>Отклоненные</h5>
                    </div>
                    <div className="col-xl-12">
                        <div className="card border-0">
                            <div className="card-body">
                                <ComplaintsListTable/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ListRejected;