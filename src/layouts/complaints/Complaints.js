import React from 'react';
import {Link,Switch,Route} from "react-router-dom";
import Dashboard from "../../pages/complaints/dashboard/Dashboard";
import List from "../../pages/complaints/list/List";
import ListIncome from "../../pages/complaints/listIncome/ListIncome";
import ListConfirmation from "../../pages/complaints/listConfirmation/ListConfirmation";
import ListInProcess from "../../pages/complaints/listPInProcess/ListInProcess";
import ListDecided from "../../pages/complaints/listDecided/ListDecided";
import ListRejected from "../../pages/complaints/listRejected/ListRejected";
import ListItem from "../../pages/complaints/list/ListItem";
import Test from "../../pages/complaints/test/Test";
import ReactTooltip from "react-tooltip";

function Complaints(props) {
    return (
        <div className='mainLayout'>
           <div className="mainLayoutLeft">
               <Link to="/" className="mainBrand">
                   <span className="icon icon-pulse"/>
                   Risk Assessment
                   <p>
                       SERVICE DESK
                   </p>
               </Link>

               <p className="title">Основной</p>
               <ul>
                   <li>
                       <Link to='/complaints' className={` ${props.history.location.pathname==='/complaints' ? 'active' : ''}`}>
                          <span className="icon icon-dashboard"/>
                           Dashboard
                       </Link>
                   </li>
                   <li>
                       <Link to='/complaints/list'  className={` ${props.history.location.pathname==='/complaints/list' ? 'active' : ''}`}>
                           <span className="icon icon-dashboard"/>
                           Все обращения
                       </Link>
                   </li>
                   <li>
                       <Link to='/complaints/list/income' className={` ${props.history.location.pathname==='/complaints/list/income' ? 'active' : ''}`}>
                          <div className="d-flex justify-content-between">
                              <div>
                                  <span className="icon icon-dashboard"/>
                                  Входящие
                              </div>
                              <span className="badge badge-pill bg-danger">25</span>
                          </div>
                       </Link>
                   </li>
               </ul>
               <p className="title">Фильтр</p>
               <ul>
                   <li>
                       <Link to='/complaints/list/confirmation' className={` ${props.history.location.pathname==='/complaints/list/confirmation' ? 'active' : ''}`}>
                           <span className="icon icon-dashboard"/>
                           На согласование
                       </Link>
                   </li>
                   <li>
                       <Link to='/complaints/list/in_process' className={` ${props.history.location.pathname==='/complaints/list/in_process' ? 'active' : ''}`}>
                           <span className="icon icon-dashboard"/>
                           В процессе
                       </Link>
                   </li>
                   <li>
                       <Link to='/complaints/list/decided' className={` ${props.history.location.pathname==='/complaints/list/decided' ? 'active' : ''}`}>
                           <span className="icon icon-dashboard"/>
                           Решенные
                       </Link>
                   </li>
                   <li>
                       <Link to='/complaints/list/rejected' className={` ${props.history.location.pathname==='/complaints/list/rejected' ? 'active' : ''}`}>
                           <span className="icon icon-dashboard"/>
                           Отклоненные
                       </Link>
                   </li>

                   <li>
                       <Link to='/complaints/list/test' className={` ${props.history.location.pathname==='/complaints/list/test' ? 'active' : ''}`}>
                           <span className="icon icon-dashboard"/>
                           Test
                       </Link>
                   </li>
               </ul>
           </div>
           <div className="mainLayoutRight">
               <div className="mainLayoutHeader ">

                      <div className="d-flex align-items-center ">
                          <div>
                              <input className="form-control me-2" type="date" name="reporting_day"/>
                          </div>
                          <div>
                              <select className="form-select select2-hidden-accessible" name="reporting_mfo">
                                  <option value="" data-select2-id="3">По всем филиалам</option>
                              </select>
                          </div>

                          <span/>

                          <p className="mb-0 my-tooltip-1" data-tip="Последный опердень">
                                 <span className="icon icon-calendar"/>
                                  Sept. 15, 2022
                          </p>

                          <p className="mb-0 my-tooltip-2" data-tip="Время последного обновление">
                              <span className="icon icon-date"/>
                              Sept. 15, 2022, 4:03 p.m.
                          </p>
                          <ReactTooltip />
                      </div>

                   <div className="dropdown d-flex justify-content-end">
                       <button type="button" className=" dropdown-toggle" data-bs-toggle="dropdown">
                           <img src="assets/navbar/user.png" alt=""/>
                           Rajabov Doston
                       </button>
                       <ul className="dropdown-menu">
                           <li><a className="dropdown-item" href="#">Выход</a></li>
                       </ul>
                   </div>
               </div>
               <div className="mainLayoutContent">
                   <Switch>
                       <Route path="/complaints" exact component={Dashboard}/>
                       <Route path="/complaints/list" exact component={List}/>
                       <Route path="/complaints/list/income" exact component={ListIncome}/>
                       <Route path="/complaints/list/confirmation" exact component={ListConfirmation}/>
                       <Route path="/complaints/list/in_process" exact component={ListInProcess}/>
                       <Route path="/complaints/list/decided" exact component={ListDecided}/>
                       <Route path="/complaints/list/rejected" exact component={ListRejected}/>
                       <Route path="/complaints/list/test" exact component={Test}/>
                       <Route path="/complaints/list/data/:id" exact component={ListItem}/>
                   </Switch>
               </div>
           </div>
        </div>
    );
}

export default Complaints;