import React from 'react';
import {Link} from "react-router-dom";
import {toast} from "react-toastify";
import axios from "axios";
import {PATH_NAME} from "../../utils/utils";

function Login(props) {
    function signUp(e) {
        e.preventDefault();
        let username = e.target.username.value;
        let password = e.target.password.value;

            axios.post(`${PATH_NAME}api/v1/auth/login`, {
                username ,
                password
            }).then((response) => {
                console.log(response)
                if (response.data.token){
                    localStorage.setItem('token', response.data.token);
                }
                if (response.data.verification_code){
                    props.history.push('/verification');
                }
                else{
                    /*localStorage.setItem('expires_at', response.data.expires_at);*/
                    toast.success("Muvaffaqqiyatli ro'yhatdan o'tdingiz");
                    props.history.push('/');
                }

            }).catch((e)=>{
                toast.error("Login yoki parol xato")
            })
    }


    return (
        <div className="login-page">
            <div className="box">
                <b>Risk Assessment System</b>
                <p className='opacity-75'>Войдите в свою учетную запись, чтобы продолжить</p>
                <div className="card">
                    <form onSubmit={signUp}>
                        <label htmlFor="username">Username*</label>
                        <input
                            required={true}
                            className="form-control mt-2 mb-3"
                            name={'username'}
                            id={'username'}
                            type="text"/>

                        <label htmlFor="password">Password*</label>
                        <input
                            required={true}
                            className="form-control mt-2 mb-3"
                            name={'password'}
                            id={'password'}
                            type="password"/>

                        <input
                            style={{transform: 'scale(1.5)'}}
                            className="mt-2 mb-3"
                            name={'check'}
                            id={'check'}
                            type="checkbox"/>
                        <label htmlFor="check" className="ms-2">Запомнить</label>

                        <button type={'submit'} className="d-block w-100 mt-4 btn btn-primary">
                            Войти
                        </button>
                    </form>

                    <div className="d-flex mt-4 footer-card">
                        <button data-bs-toggle="modal" data-bs-target="#myModal">Восстановление пароля</button>
                        <span/>
                        <Link to={'/register'}> Зарегистрироваться в системе</Link>
                    </div>
                </div>
            </div>


            <div className="modal fade" id={'myModal'}>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header bg-dark">
                            <b className="text-white">Сменить пароль</b>
                            <button type="button" className="btn-close btn-close-white" data-bs-dismiss="modal"/>
                        </div>
                        <div className="modal-body">
                            <label htmlFor="userName">Имя ползователя</label>
                            <input
                                name={'userName'}
                                id={'userName'}
                                className="form-control"
                                type="text"/>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-light btn-sm" data-bs-dismiss="modal">Закрыть
                            </button>
                            <button type="submit" className="btn btn-primary btn-sm" data-bs-dismiss="modal">Сменить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;