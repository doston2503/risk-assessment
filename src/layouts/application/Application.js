import React from 'react';
import {Link, Route, Switch} from "react-router-dom";
import Dashboard from "../../pages/application/dashboard/Dashboard";
import DashboardList from "../../pages/application/dashboard/DashboardList";

function Application(props) {
    return (
        <div className='mainLayout'>
            <div className="mainLayoutLeft">
                <Link to="/" className="mainBrand">
                    <span className="icon icon-pulse"/>
                    Risk Assessment
                    <p>
                        ОБРАЩЕНИЯ
                    </p>
                </Link>

                <p className="title">ОБРАЩЕНИЯ</p>
                <ul>
                    <li>
                        <Link to='/application'
                              className={` ${props.history.location.pathname === '/application' ? 'active' : ''}`}>
                            <span className="icon icon-dashboard"/>
                            Dashboard
                        </Link>
                    </li>
                </ul>
            </div>
            <div className="mainLayoutRight">
                <div className="mainLayoutHeader ">


                    <div className="dropdown d-flex justify-content-end">
                        <button type="button" className=" dropdown-toggle" data-bs-toggle="dropdown">
                            <img src="assets/navbar/user.png" alt=""/>
                            Rajabov Doston
                        </button>
                        <ul className="dropdown-menu">
                            <li><a className="dropdown-item" href="#">Выход</a></li>
                        </ul>
                    </div>
                </div>
                <div className="mainLayoutContent">
                    <Switch>
                        <Route path="/application" exact component={Dashboard}/>
                        <Route path="/application/:id" exact component={DashboardList}/>
                    </Switch>
                </div>
            </div>
        </div>
    );
}

export default Application;