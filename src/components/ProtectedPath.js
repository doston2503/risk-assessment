import React from 'react';
import {Redirect} from "react-router-dom";

function ProtectedPath({children}) {
    const token = localStorage.getItem('token');

    if (!token) {
        localStorage.clear();
        return <Redirect to='/login'/>
    }

    return children;
}

export default ProtectedPath;